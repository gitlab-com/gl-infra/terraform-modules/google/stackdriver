# GitLab.com Stackdriver Terraform Module

## What is this?

This module configures Stackdriver in a GCP project.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 4.0.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | >= 4.0.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [google_logging_project_exclusion.my-exclusion](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/logging_project_exclusion) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_project"></a> [project](#input\_project) | GCP Project (optional) | `string` | `null` | no |
| <a name="input_sd_log_filters"></a> [sd\_log\_filters](#input\_sd\_log\_filters) | Map of name -> filter, for logs to exclude | `map(string)` | `{}` | no |

## Outputs

No outputs.
<!-- END_TF_DOCS -->

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
