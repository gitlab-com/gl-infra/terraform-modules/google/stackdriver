############################
# Stackdriver log exclusions
############################

resource "google_logging_project_exclusion" "my-exclusion" {
  for_each = var.sd_log_filters

  name        = each.key
  description = "Exclude logs, managed by terraform"

  filter  = each.value
  project = var.project
}
