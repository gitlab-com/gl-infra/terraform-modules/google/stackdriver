variable "sd_log_filters" {
  type        = map(string)
  description = "Map of name -> filter, for logs to exclude"
  default     = {}
}

variable "project" {
  type        = string
  description = "GCP Project (optional)"
  default     = null
}
